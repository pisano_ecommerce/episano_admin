<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('norden', 100);
            $table->integer('idcli');
            $table->date('fecha');
            $table->float('importe');
            $table->integer('medio_pago');
            $table->integer('cuotas');
            $table->float('valor_desc');
            $table->float('valor_envio');
            $table->string('tipo_envio', 100);
            $table->string('estado', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('e_pedidos');
    }
}
