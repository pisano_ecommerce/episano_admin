<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->string('apellido', 100);
            $table->integer('dni');
            $table->string('telefono', 100);
            $table->string('e_mail', 100);
            $table->string('domicilio', 100);
            $table->integer('nro_domicilio');
            $table->string('depto', 1000);
            $table->string('ciudad', 100);
            $table->string('c_postal', 100);
            $table->string('provincia', 100);
            $table->string('pais', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('e_clientes');
    }
}
