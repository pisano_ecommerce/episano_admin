<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEpedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('e_pedidos', function($table) {
            $table->integer('nro_factura');
            $table->integer('nro_presupuesto');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('e_pedidos', function($table) {
            $table->dropColumn('nro_factura');
            $table->dropColumn('nro_presupuesto');
        });
    }
}
