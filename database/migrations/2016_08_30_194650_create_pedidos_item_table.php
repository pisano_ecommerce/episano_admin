<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_pedido_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku', 100);
            $table->integer('cantidad');
            $table->float('precio');
            $table->string('wharehouseid', 100);
            $table->string('orden_id', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('e_pedido_item');
    }
}
