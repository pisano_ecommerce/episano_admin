<?php

use Illuminate\Database\Seeder;
use App\EUsers;

class EUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EUsers::create(array(
            'first_name'  => 'admin',
            'last_name' => 'admin',
            'email'     => 'admin@pisano.com.ar',
            'activated' => 1,
            'password' => Hash::make('admin')
         )); // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
    }
}
