<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\models\Act_Stock_Precio;

class PrecioController extends Controller
{
    /*Carga de precios de productos al e-commerce*/
	public function cargarPrecios() {
		
	}

	/*Actualiza Precio y Stock de un producto en VTEX*/
	public function actualizar_precio_stock() {

		\Log::info('Consultando datos a actualizar sobre productos...');

		/*Traigo de la tabla de actualizacion de productos los registros a actualizar*/
		/*Valido si trae algun registro, si no no hay nada, entonces termina la consulta*/
		$registros_actualizar = Act_Stock_Precio::consultar_registros_actualizar();
		
		if (count($registros_actualizar) > 0) {
			/*recorro los registros para actualizar*/
			foreach ($registros_actualizar as $key => $value) {
				/*traigo de VTEX la info del producto a actualizar para obtener el id de producto*/
					$info_producto =  consultar_producto_by_refId($value->sku);

					$id_producto = $info_producto->Id;
					
					/*Valido la info a actualizar*/
					if (!is_null($value->stock)) {
						$stock = $value->stock;
						/*Actualizo el stock del producto*/
						actualizarStockProducto($id_producto, $stock, 0);
						\Log::info('Se actualizo el Stock correctamente para el sku '.$value->sku);
					
					}

					if (!is_null($value->precio) && !is_null($value->precio_lista)) {
						$precio = $value->precio;
						$precio_lista = $value->precio_lista;
						$info_precio_producto = consultPriceListBySku($id_producto);
						$id = $info_precio_producto[0]->id;
						
						$array_update_price['id'] = $id;
						$array_update_price['itemId'] = (int)$id_producto;
						$array_update_price['salesChannel'] = 1;
						$array_update_price['price'] = (int)$precio;
						$array_update_price['listPrice'] = (int)$precio_lista;

						$result = updatePriceListBySku($array_update_price);
						\Log::info('Se actualizo el precio correctamente para el sku '.$value->sku);
					}

					if (!is_null($value->descripcion)) {
						$descripcion = $value->descripcion;

					}

					$result = Act_Stock_Precio::eliminar_registro_actualizacion($value->id);

					if ($result) {
						\Log::info('Se elimino correctamente el registro '.$value->id);
						
					} else {
						\Log::info('No se pudo eliminar el registro '.$value->id);

					}

			}

		} else {
			\Log::info('No se encontraron registros para actualizar');

		}

	}

}


