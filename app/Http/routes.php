<?php

/*Route::auth();

Route::get('login', 'AuthController@getLogin');
Route::post('login', 'AuthController@postLogin');
Route::get('logout', 'AuthController@getLogout');

Route::controllers([
   'password' => 'Auth\PasswordController',
]);
	

Route::group(['before' => 'auth'], function()
{
    Route::get('home', 'HomeController@getIndex');

	Route::get('/', function () {
    	return View::make('pages.home');
	});
});*/

/*API'S*/
Route::get('api/pedidos/listarPedidos' , 'Api\PedidoController@listarPedidos');

Route::get('api/pedidos/validarEstadoPedidos', 'Api\PedidoController@validarEstadoPedidos');

Route::get('api/stock/actualizacionStock', 'Api\StockController@actualizacionStock');

Route::get('api/precio/actualizar_precio_stock', 'Api\PrecioController@actualizar_precio_stock');


Route::group(['middleware' => ['web']], function () {
    Route::get('/', ['middleware'=> 'auth', 'uses' => 'HomeController@getIndex']);
    Route::get('home', ['middleware'=> ['auth'], 'uses' => 'HomeController@getIndex']);

    /*Rutass del menú izquierdo*/
    Route::get('precios', ['middleware'=> ['auth'], 'uses' => 'PrecioController@getIndex']);
    Route::get('pedidos', ['middleware'=> ['auth'], 'uses' => 'PedidoController@getIndex']);
    Route::get('stock', ['middleware'=> ['auth'], 'uses' => 'StockController@getIndex']);

    /*Rutas de Pedidos*/
    Route::get('pedidos/listarPedidos', ['middleware'=> ['auth'], 'uses' => 'PedidoController@listarPedidos']);
    Route::get('pedidos/detalle/{orderId}', ['middleware'=> ['auth'], 'uses' => 'PedidoController@detallePedido']);

    /*AJAX*/ 

        /*Pedidos*/
    Route::get('pedidos/showPedidosDataTable/', ['middleware'=> ['auth'], 'uses' => 'PedidoController@showPedidosDataTable']);

        /*Stock*/
    Route::get('/stock', ['middleware'=> ['auth'], 'uses' => 'StockController@getIndex']);
    Route::get('/stock/showStockDataTable', ['middleware'=> ['auth'], 'uses' => 'StockController@showStockDataTable']);


    /*Rutas de Precios*/
    Route::get('precios/cargarPrecios', ['middleware'=> ['auth'], 'uses' => 'PrecioController@cargarPrecios']);
    Route::get('precios/listaPrecioSku/{sku}', ['middleware'=> ['auth'], 'uses' => 'PrecioController@listaPrecioSku']);

    Route::get('login', 'AuthController@getLogin');
    Route::post('login', 'AuthController@postLogin');
    Route::get('logout', 'AuthController@getLogout');

    // Registration Routes...
    Route::get('register', 'AuthController@showRegistrationForm');
    Route::post('register', 'AuthController@register');

});
	
	





