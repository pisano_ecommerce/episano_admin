<?php  

	function pedido($orderId = null) {

		if (!is_null($orderId)) {
			
			/*Inicio curl*/
	        $ch = curl_init();

	        /*Seteo las opciones de curl basicas*/
	        curl_setopt($ch,CURLOPT_URL, "https://pisano.vtexcommercestable.com.br/api/oms/pvt/orders/".$orderId);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
	        

	        /*Armo el header con la info para conectarse correctamente a la API de VTEX*/
	        $headers = array();
	        $headers[] = 'Accept: application/json';
	        $headers[] = 'Content-Type: application/json';
	        $headers[] = 'X-VTEX-API-AppToken: IKPIFZAHBQNKGQEJKRFTBCRAUCFQBKLOUGJDIGMPVFNPTCWAFIITGROGTPODUKGQEZQAGBAEEQYWPBGRFFLSBSICRDKTSJUKYWLUMWTDHEPPTSQRIWYPZNOLQNJDRHMV';
	        $headers[] = 'X-VTEX-API-AppKey: vtexappkey-pisano-IKPIFZ';

	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	        $server_output = curl_exec ($ch);
	        if (curl_errno($ch)) {
	             die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
	        } 
	        curl_close($ch); 
	        
	        return json_decode($server_output);

		} else {
			return "No se ha indicado un ID de pedido, por favor indique alguno.";

		}

	}

	function listaPedidos() {

        /*Inicio curl*/
        $ch = curl_init();

        /*Seteo las opciones de curl basicas*/
        curl_setopt($ch,CURLOPT_URL, "https://pisano.vtexcommercestable.com.br/api/oms/pvt/orders?f_Status=ready-for-handling&amp;orderBy=creationDate,desc&amp;per_page=100&amp;page=1");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        

        /*Armo el header con la info para conectarse correctamente a la API de VTEX*/
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'X-VTEX-API-AppToken: IKPIFZAHBQNKGQEJKRFTBCRAUCFQBKLOUGJDIGMPVFNPTCWAFIITGROGTPODUKGQEZQAGBAEEQYWPBGRFFLSBSICRDKTSJUKYWLUMWTDHEPPTSQRIWYPZNOLQNJDRHMV';
        $headers[] = 'X-VTEX-API-AppKey: vtexappkey-pisano-IKPIFZ';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $server_output = curl_exec ($ch);
        if (curl_errno($ch)) {
             die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        } 

        curl_close($ch); 

        /*Listado de Pedidos*/
        return json_decode($server_output);
       
    }

    function desencriptar_email($email) {
  
    	if (!is_null($email)) {
			
			/*Inicio curl*/
	        $ch = curl_init();

	        /*Seteo las opciones de curl basicas*/
	        curl_setopt($ch,CURLOPT_URL, "http://conversationtracker.vtex.com.br/api/pvt/emailMapping/?alias=".$email.'&an=pisano');
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
	        

	        /*Armo el header con la info para conectarse correctamente a la API de VTEX*/
	        $headers = array();
	        $headers[] = 'Accept: application/json';
	        $headers[] = 'Content-Type: application/json';
	        $headers[] = 'X-VTEX-API-AppToken: IKPIFZAHBQNKGQEJKRFTBCRAUCFQBKLOUGJDIGMPVFNPTCWAFIITGROGTPODUKGQEZQAGBAEEQYWPBGRFFLSBSICRDKTSJUKYWLUMWTDHEPPTSQRIWYPZNOLQNJDRHMV';
	        $headers[] = 'X-VTEX-API-AppKey: vtexappkey-pisano-IKPIFZ';

	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	        $server_output = curl_exec ($ch);
	        if (curl_errno($ch)) {
	             die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
	        } 
	        curl_close($ch); 
	        
	        return json_decode($server_output);

		} else {
			return "No se ha indicado un email, por favor indique alguno.";

		}

    }

    function actualizarEstadoPedido($ordenId = null, $status = null, $info_pedido = null) {

    	if (!is_null($ordenId) && !is_null($status)) {

			$data_string = array();
			/*Inicio curl*/
	        $ch = curl_init();

	        /*Seteo las opciones de curl basicas*/
	        curl_setopt($ch,CURLOPT_URL, "https://pisano.vtexcommercestable.com.br/api/oms/pvt/orders/".$ordenId."/".$status);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);

	        $headers = array();

	        if ($status == "invoice" && !is_null($info_pedido)) {

	        	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	        	$data_string = $info_pedido;
	        	//$header[] = 'Content-Length: ' . strlen($data_string);
	        }

	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	        

	        /*Armo el header con la info para conectarse correctamente a la API de VTEX*/
	        $headers[] = 'Accept: application/json';
	        $headers[] = 'Content-Type: application/json';
	        $headers[] = 'X-VTEX-API-AppToken: IKPIFZAHBQNKGQEJKRFTBCRAUCFQBKLOUGJDIGMPVFNPTCWAFIITGROGTPODUKGQEZQAGBAEEQYWPBGRFFLSBSICRDKTSJUKYWLUMWTDHEPPTSQRIWYPZNOLQNJDRHMV';
	        $headers[] = 'X-VTEX-API-AppKey: vtexappkey-pisano-IKPIFZ';

	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	        $server_output = curl_exec ($ch);
	        if (curl_errno($ch)) {
	             return curl_error($ch);
	        } 
	        curl_close($ch); 
	        
	        return $server_output;

		} else {
			return "No se ha indicado un ID de pedido o un Estado, por favor indique ambos.";

		}

    }


    


