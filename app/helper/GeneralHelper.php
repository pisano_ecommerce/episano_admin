<?php 
	
	function formatear_precio($precio) {

		if (!is_null($precio)) {
			if ($precio === 0) {
				$importe_array_formateado = $precio.".00";
				return $importe_array_formateado;
			}
			$substr_importe = substr($precio, -2);
		 	$importe_array = str_split($precio);
		 	$cant_caracteres_importe_array = count($importe_array);
		 	$pos_to_change = $cant_caracteres_importe_array -2;
			$importe_array[$pos_to_change] = ".".$importe_array[$pos_to_change];
			$importe_array_formateado = implode($importe_array);

			return $importe_array_formateado;

		} else {
			$importe_array_formateado = 1;
			return $importe_array_formateado;

		}
	}

	function consultar_producto_by_refId($refId = null) {
		if (!is_null($refId)) {
			
			/*Inicio curl*/
	        $ch = curl_init();

	        /*Seteo las opciones de curl basicas*/
	        curl_setopt($ch,CURLOPT_URL, "http://pisano.vtexcommercestable.com.br/api/catalog_system/pvt/products/productgetbyrefid/".$refId);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
	        

	        /*Armo el header con la info para conectarse correctamente a la API de VTEX*/
	        $headers = array();
	        $headers[] = 'Accept: application/json';
	        $headers[] = 'Content-Type: application/json';
	        $headers[] = 'X-VTEX-API-AppToken: IKPIFZAHBQNKGQEJKRFTBCRAUCFQBKLOUGJDIGMPVFNPTCWAFIITGROGTPODUKGQEZQAGBAEEQYWPBGRFFLSBSICRDKTSJUKYWLUMWTDHEPPTSQRIWYPZNOLQNJDRHMV';
	        $headers[] = 'X-VTEX-API-AppKey: vtexappkey-pisano-IKPIFZ';

	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	        $server_output = curl_exec ($ch);
	        if (curl_errno($ch)) {
	             die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
	        } 
	        curl_close($ch); 
	        
	        return json_decode($server_output);

		}
	}


?>