<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;


class Pedidos extends Model
{
    protected $table = 'e_pedidos';


    /*Muestra todos los pedidos de la base de datos que hayan pasado 30 minutos despues de su creacion en VTEX*/
    public static function mostrarPedidos() {
    	$allPedidos = DB::table('e_pedidos')->select(['id', 'norden', 'idcli', 'importe', 'fecha', 'estado', 'nro_factura', 'nro_presupuesto', 'nro_mp', 'nro_decidir', 'verificacion_transaccion'])->get();
    	//->whereRaw('TIMESTAMPDIFF(MINUTE, fecha, now()) >= 30')
    
    	$allPedidos = collect($allPedidos);

    	return $allPedidos;

    }

    public static function getCantPedidos() {
        $allPedidos = DB::table('e_pedidos')->select(['id', 'norden', 'idcli', 'importe', 'fecha', 'estado', 'nro_factura', 'nro_presupuesto'])->get();
        $count = Pedidos::count();
        return $count;
    }

    public static function getInfoPedido($orderId) {
    	$info_pedido = DB::table('e_pedidos')
        ->select('e_pedidos.*', 'e_pedido_item.*', 'stock0.detalle', 'stock0.stock')
        ->join('e_pedido_item', 'e_pedido_item.orden_id', '=', 'e_pedidos.norden')
        ->join('stock0', 'stock0.codigo', '=', 'e_pedido_item.sku')
        ->where('norden', $orderId)
        ->get();
        
    	return $info_pedido;

    }

    public static function validarCantidadPedidos($cant = null) {
    	if (!is_null($cant)) {
    		$cantidad_actual = Pedidos::count();

    		if($cantidad_actual > $cant) {
    			return true;

    		} else {
    			return $cant;

    		}
    	}
    }

    public static function validarExistenciaPedido($nro_pedido) {
    	$info_pedido = DB::table('e_pedidos')->where('norden', $nro_pedido)->get();

    	return $info_pedido;

    }

    public static function guardarPedido($info_pedido = array()) {
    	$pedido = new Pedidos();
    	$existencia_pedido = $pedido::validarExistenciaPedido($info_pedido['norden']);

    	if (count($existencia_pedido === 0)) {
    		if (!is_null($info_pedido) && is_array($info_pedido)) {
	    		
	    		$pedido->norden = $info_pedido['norden'];
	    		$pedido->idcli = $info_pedido['idcli'];
	    		$pedido->fecha = $info_pedido['fecha'];
	    		$pedido->importe = $info_pedido['importe'];
	    		$pedido->medio_pago = $info_pedido['medio_pago'];
	    		$pedido->cuotas = $info_pedido['cuotas'];
	    		$pedido->valor_desc = $info_pedido['valor_desc'];
	    		$pedido->valor_envio = $info_pedido['valor_envio'];
	    		$pedido->tipo_envio = $info_pedido['tipo_envio'];
	    		$pedido->estado = $info_pedido['estado'];
                $pedido->nro_mp = $info_pedido['nro_mp'];
                $pedido->verificacion_transaccion = $info_pedido['verificacion_transaccion'];

	    		$pedido->save();

    		} else {

    		}

    	}

    }

    public static function buscarPedidosCanceladosFacturados() {

        /*$pedidos_cancelados_facturados = DB::table('e_pedidos')
            ->join('e_pedido_item', 'e_pedidos.norden', '=', 'e_pedido_item.orden_id')
            ->select('e_pedidos.*')
            ->where('e_pedidos.estado', "canceled")
            ->orWhere('e_pedidos.estado', "invoiced")
            ->where('e_pedidos.notificado_ecommerce', "0")
            ->toSql();*/

            $pedidos_cancelados_facturados = DB::table('e_pedidos')
            ->join('e_pedido_item', 'e_pedidos.norden', '=', 'e_pedido_item.orden_id')
            ->select('e_pedidos.*')
            ->where('e_pedidos.notificado_ecommerce', 0)
            ->where(function($query) {
                $query->where('e_pedidos.estado', "canceled")->orWhere('e_pedidos.estado', "invoiced");
            }) 
            ->get();

        return $pedidos_cancelados_facturados;
      
    }

    public static function getInfoPedidoItems($orderId) {

        $info_pedido = DB::table('e_pedido_item')
        ->select('e_pedido_item.sku', 'e_pedido_item.precio', 'e_pedido_item.cantidad')
        ->where('e_pedido_item.orden_id', $orderId)
        ->get();
        return $info_pedido;

    }

    public static function actualizarProcesamientoPedido($orderId = null) {

        $pedido = Pedidos::where('e_pedidos.norden', $orderId)->first();
        
        if (count($pedido) >= 1) {
            // Seteamos un nuevo titulo
            $pedido->notificado_ecommerce = "1";
 
            // Guardamos en base de datos
            $pedido->save();
        }

        
    }

}





    

    
