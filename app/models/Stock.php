<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Stock extends Model
{
    protected $table = 'stock0';

    /*Funcion que muestra el listado total de stock por productos*/
    public static function getStock() {
    	$allStock = DB::table('stock0')->select(['codigo', 'producto',  'detalle', 'costo', 's_deposito', 'stock'])->get();

    	$allStock = collect($allStock);
        
    	return $allStock;
    }

    /*Funcion que trae el stock de un producto por sku para luego actualizarlo en VTEX*/
    public static function getStockBySku($sku) {
        if (!is_null($sku)) {
            $stock = DB::table('stock0')->select(['stock'])->where('codigo', $sku)->get();
            return $stock;
        } 
    }    
}
