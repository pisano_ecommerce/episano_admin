<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Act_Stock_Precio extends Model
{	

	protected $table = 'e_act_stock_precio';

    //obtiene de la tabla los registros con los datos a actualizar por producto
    public static function consultar_registros_actualizar() {

    	$registros_actualizar = DB::table('e_act_stock_precio')->get();
    	return $registros_actualizar;
    }

    public static function eliminar_registro_actualizacion($id_registro = null) {
    	if (!is_null($id_registro)) {
    		$act_stock_precio = Act_Stock_Precio::find($id_registro);
    		$result = $act_stock_precio->delete();
    		return $result;
    	}
    }
}
