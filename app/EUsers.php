<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class EUsers extends Model implements AuthenticatableContract
{	
	use Authenticatable;
    protected $table = 'e_users';
}
