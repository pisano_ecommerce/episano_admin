@extends('layouts.home-layout')
@section('content')
<div class="box-body table-responsive">
    <table id="stock_list" class="table table-bordered table-hover">
    	<thead>
                <tr>
                    <th>Sku</th>
                    <th>Producto</th>
                    <th>Detalle</th>
                    <th>Costo</th>
                    <th>Depósito</th>
                    <th>Stock</th>
                </tr>
            </thead>
    </table>
</div>
@stop

@section('page-script')
<script type="text/javascript">
$(function() {
    $('#stock_list').dataTable({
        "language": {
                "url": "assets/datatable_spanish.json"
            },
        "processing": true,
        "serverSide": true,
        "ajax": "{!! url('/stock/showStockDataTable/') !!}",
        "columns": [
            {"data": "codigo"},
            {"data": "producto"},
            {"data":"detalle"},
            {"data":"costo"},
            {"data":"s_deposito"},
            {"data":"stock"},
        ],
    });
});
</script>
@stop

