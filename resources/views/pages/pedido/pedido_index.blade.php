@extends('layouts.home-layout')
@section('content')
	<input type="hidden" id="cant_pedidos" name="cant_pedidos" value="{{ $cant_pedidos }}">
	<div class="form-group row">
      <div class="col-xs-12">
        <div class="col-xs-10">
          <h3 style="margin-top:0px;" class="h3-detail-pedido">Lisado de Pedidos</h3>
        </div>

        <div class="col-xs-2">
          <a href="{!! url('/pedidos'); !!}" class="btn btn-block btn-warning">Actualizar Lista</a>
        </div>
          
      </div>
    </div>
	
	<div class="box-body table-responsive">
	    <table id="orders_list" class="table table-bordered table-hover">
	    	<thead>
	                <tr>
	                	<th>N° Pedido</th>
	                	<th>CLiente</th>
	                    <th>Fecha</th>
	                    <th>Importe</th>
	                    <th>Factura</th>
	                    <th>Presupuesto</th>
	                    <th>Estado</th>
                      <th>N° MercadoPago</th>
                      <th>N° Decidir</th>
                      <th>Errores</th>

	                </tr>
	            </thead>
	    </table>
	</div>
@stop

@section('page-script')

<script type="text/javascript">
$(function() {
	window.ultimo_registro_pedido = $('#cant_pedidos').val();
	
	var hostname = window.location.hostname;
   	var table = $('#orders_list').dataTable({
    	"language": {
                "url": "assets/datatable_spanish.json"
            },
        "processing": true,
        "serverSide": true,
        "ajax": "{!! url('/pedidos/showPedidosDataTable/') !!}",
        "rowCallback": function( row, data) {

        	$(row).attr( "data-href", "{!! url('/pedidos/detalle/"+data.norden+"') !!}" );
        
        	if ( data.estado == "canceled" ) {
      			$('td:eq(6)', row).addClass( "td_status_canceled" );
      			$('td:eq(6)', row).html( "CANCELADO" );
      		}
      		if ( data.estado == "invoiced" ) {
      			$('td:eq(6)', row).addClass( "td_status_invoiced" );
      			$('td:eq(6)', row).html( "FACTURADO" );
      		}
      		if ( data.estado == "ready-for-handling" ) {
      			$('td:eq(6)', row).addClass( "td_status_handling" );
      			$('td:eq(6)', row).html( "EN PROGRESO" );
      		}

          if ( data.verificacion_transaccion == 0 ) {
            $('td:eq(9)', row).addClass( "td_status_invoiced" );
            $('td:eq(9)', row).html( "OK!" );
          } else if (data.verificacion_transaccion == 1)  {
            $('td:eq(9)', row).addClass( "td_status_invoiced" );
            $('td:eq(9)', row).html( "FALLOS!");
          }


            $(row).addClass('row-order');
        },
        
        "columns": [
            {"data": "norden"},
            {"data": "idcli"},
            {"data":"fecha"},
            {"data":"importe"},
            {"data":"nro_factura"},
            {"data":"nro_presupuesto"},
            {"data":"estado"},
            {"data":"nro_mp"},
            {"data":"nro_decidir"},
            {"data":"verificacion_transaccion"}
        ],
    });

    $('.row-order').on("click", function(){
    	
    	});

	$('#orders_list tbody').on('click', 'tr[data-href]', function () {
        document.location = $(this).data('href');
        
    });
	
	setInterval("actualizar_listado_pedido()", 900000);

});

function actualizar_listado_pedido() {
		var ultimo_registro_pedido = $('#cant_pedidos').val();
		$.get("pedidos/validarPedidosNuevos/"+ultimo_registro_pedido+"",function(response){
			console.log(response);
			if (response == "0") {
				location.reload();
			}
		});
	}




</script>
@stop