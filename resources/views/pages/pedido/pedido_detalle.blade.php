@extends('layouts.home-layout')
@section('content')

@if(count($order_info) > 0)
  <div id="container" class="box-body">

      <div class="form-group row">
        <div class="col-xs-12">
          <div class="col-xs-10">
            <h3 style="margin-top:0px;" class="h3-detail-pedido">Detalle de Pedido</h3>
          </div>

          <div class="col-xs-2">
            <a href="{!! url('/pedidos'); !!}" class="btn btn-block btn-warning">Volver</a>
          </div>
            
        </div>
      </div>

      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#infoPedido">Info Pedido</a></li>
        <li><a data-toggle="tab" href="#productos">Productos</a></li>
      </ul>

      <div class="tab-content">

        <div id="infoPedido" class="tab-pane fade in active">
            
              <div class="box box-primary col-xs-12">

                  <div class="col-xs-6">

                    <div class="form-group row margin-first-row-detail-pedido">
                      <label for="example-text-input" class="col-xs-3 col-form-label detail-order-label">N° de Pedido</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="{{ $order_info[0]->norden }}" id="orderId">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Fecha</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="{{ $order_info[0]->fecha }}" id="date_order">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Presupuesto</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="tel" value="{{ $order_info[0]->nro_presupuesto }}" id="presupuesto_order">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Importe</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="${{ $order_info[0]->importe }}" id="importe_order">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Cuotas</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="{{ $order_info[0]->cuotas }}" id="cuota_order">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Valor de envío</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="${{ $order_info[0]->valor_envio }}" id="valor_envio_order">
                      </div>
                    </div> 

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Estado</label>
                      <div class="col-xs-9">
                        @if ($order_info[0]->estado == 'ready-for-handling')
                             <input disabled class="form-control" type="text" value="EN PROCESO" id="status_order">
                          @elseif ($order_info[0]->estado == 'invoiced')
                            <input disabled class="form-control" type="text" value="FACTURADO" id="status_order">
                          @elseif ($order_info[0]->estado == 'canceled')
                            <input disabled class="form-control" type="text" value="CANCELADO" id="status_order">
                      @endif
                      </div>
                    </div>          
                    
                  </div>

                  <div class="col-xs-6">

                    <div class="form-group row margin-first-row-detail-pedido">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Importe</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="${{ $order_info[0]->importe }}" id="importe_order">
                      </div>
                    </div>
                      
                    <div class="form-group row margin-first-row-detail-pedido">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Cliente</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="search" value="{{ $order_info[0]->idcli }}" id="cliente_order">
                      </div>
                    </div> 

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">N° de Factura</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="{{ $order_info[0]->nro_factura }}" id="factura_order">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Medio de Pago</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="{{ $order_info[0]->medio_pago }}" id="medio_pago_order">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Tipo de envío</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="{{ $order_info[0]->tipo_envio }}" id="tipo_envio_order">
                      </div>
                    </div> 

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Descuento</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="${{ $order_info[0]->valor_desc }}" id="descuento_order">
                      </div>
                    </div> 

                  </div>
            
              </div>

        </div>


        <div id="productos" class="tab-pane fade">

            @foreach ($order_info as $producto) 
              
              <div class="box box-primary col-xs-12">

                  <div class="col-xs-6">

                    <div class="form-group row margin-first-row-detail-pedido">
                      <label for="example-text-input" class="col-xs-3 col-form-label detail-order-label">Código</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="{{ $producto->sku }}" id="producto_id_sku">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Cantidad</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="{{ $producto->cantidad }}" id="date_order">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Stock</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="tel" value="{{ $producto->stock }}" id="stock_prducto">
                      </div>
                    </div>

                  </div>

                  <div class="col-xs-6">

                    <div class="form-group row margin-first-row-detail-pedido">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Precio</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="text" value="${{ $producto->precio }}" id="importe_producto">
                      </div>
                    </div>
                      
                    <div class="form-group row margin-first-row-detail-pedido">
                      <label for="" class="col-xs-3 col-form-label detail-order-label">Detalle</label>
                      <div class="col-xs-9">
                        <input disabled class="form-control" type="search" value="{{ $producto->detalle }}" id="detalle_producto">
                      </div>
                    </div> 
                    
                  </div>
            
              </div>

            @endforeach


         
        </div>


      </div>


  </div>


@else 

<h3 style="text-align:center;">No existen productos cargados para este pedido</h3>

@endif

@stop

@section('page-script')

<script type="text/javascript">
$(function() {
    
});
</script>
@stop